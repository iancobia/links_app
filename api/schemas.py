from typing import Optional, List

from pydantic import BaseModel

class User(BaseModel):
    username: str
    password: str
    favorites: List[str] = []