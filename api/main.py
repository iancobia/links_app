from typing import Optional
import json
import logging

from fastapi import FastAPI, Request
from pydantic import BaseModel
from pymongo import MongoClient

from models import User

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)
FORMAT = '[ %(levelname)s ] - %(asctime)s %(message)s'
logging.basicConfig(format=FORMAT)

client = MongoClient('db', 27017)
db = client['dev']

app = FastAPI()

@app.get("/")
async def api_check():
    return {"message": "Hi There"}

@app.post("/user/")
async def create_user(user: User, req: Request):
    """
    class User(BaseModel):
        username: str
        password: str
        favorites: Optional[list]
    """
    user_db = db['users']
    user = user.dict()
    logger.info(req.headers)

    if user_db.find_one({"username": user["username"]}):
        err = "User exists"
        logger.error(err)
        
        return {"error": err}

    res = user_db.insert_one(user)
    
    return {"message": f"{user['username']} added"}