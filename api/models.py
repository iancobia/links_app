from typing import Optional, List

from pydantic import BaseModel

from schemas import User

class DBUser(User):
    username: str
    password: str
    favorites: List[str] = []


class Link(BaseModel):
    name: str
    link: List[str] = []
    category: str
    icon: Optional[str]


class Category(BaseModel):
    name: str
    icon: Optional[str]